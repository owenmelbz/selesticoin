var login = {

	init: function(){
		this.preLayout();
        $.loginScroller.scrollingEnabled = false;
	},

	bindEvents: function(){

		$.loginName.addEventListener('return', function(){
			$.loginPassword.focus();
		});
		$.loginPassword.addEventListener('return', login.login);
		$.loginButton.addEventListener('click', login.login);

		$.regName.addEventListener('return', function(){
			$.regPassword.focus();
		});
		$.regPassword.addEventListener('return', login.register);
		$.regButton.addEventListener('click', login.register);
		
		//Sequence Unlock
		(function( view ){
		    
		    if(!app.isAndroid){
                var Step = {
                    up: false,
                    down: false,
                    dbl: false
                };
    		    
    		    view.addEventListener('swipe', function( e ){
    		        
    		        
    		        if( e.direction == "up" ){
    		            
    		            if( Step.up == false ){
    		                Step.up = true;
    		                Step.down = false;
                            Step.dbl = false;
    		            } else if( Step.up == true || Step.down == true || Step.dbl == true){
    		                Step.up = false;
                            Step.down = false;
                            Step.dbl = false;
    		            }
    		            
    		        } else if( e.direction == "down" ){
    		          
    		              if( Step.up == true && Step.down == false){
    		                  Step.up = true;
    		                  Step.down = true;
                              Step.dbl = false;
    		              }
    		              else {
    		                    Step.up = false;
                                Step.down = false;
                                Step.dbl = false;
    		              }
    		        } else {
    		            Step.up = false;
                        Step.down = false;
                        Step.dbl = false;
    		        }
    		        
    		        Log.warn( Step );
    		       		          
    		    });
    		    
    		    view.addEventListener('dblclick', function( e ){
                    
                    if( Step.up && Step.down && Step.dbl == false){
                        view.scrollingEnabled = true;
                        
                        Step.up = true;
                        Step.down = true;
                        Step.dbl = true;
                        
                    } else {
                        Step.up = false;
                        Step.down = false;
                        Step.dbl = false;
                    }
                    
                    Log.warn( Step );
                    
    		    });
    		    
		    } else {
		        
		        var floorCount = 0;
		        
		        $.floortrigger.addEventListener('click', function(){
		           floorCount++;
		           
		           if( floorCount > 15 ){
		               view.scrollingEnabled = true;
		               alert('Ooooch!');
		           };
		        });
		    }
		    
		})( $.loginScroller );
		
		if( app.allowedAuth ){
		    $.loginScroller.scrollingEnabled = true;
		}

	},

	preLayout: function(){

		$.loginName.value = Titanium.App.Properties.getString('last_username', '');

		this.postLayout();
	},

	postLayout: function(){
		this.bindEvents();
	},

	login: function(){

		var user = $.loginName.value;
		var pass = $.loginPassword.value;

		if(!user) return alert('Please enter a username');
		if(!pass) return alert('Please enter a password');

		Loader.start('Trying');

		new XHR().post( Api.auth.login , {
			username: user,
			password: pass,
			device: app.installId,
			token: app.Notifications.token
		}, function( resp ){
			app.user = JSON.parse( resp.data );
			Titanium.App.Properties.setObject('user', app.user);
			app.loginSuccess();
			Loader.stop();
		}, function( resp ){
			Loader.stop();
			return alert( respw );
		});
	},

	register: function(){

		var user = $.regName.value;
		var pass = $.regPassword.value;

		if(!user) return alert('Please enter a username');
		if(!pass) return alert('Please enter a password');

		Loader.start('Registering');

		new XHR().post( Api.auth.register , {
			username: user,
			password: pass,
			device: app.installId,
			token: app.Notifications.token
		}, function( resp ){
			app.user = JSON.parse( resp.data );
			Titanium.App.Properties.setObject('user', app.user);
			app.loginSuccess();
			Loader.stop();
		}, function( resp ){
			Loader.stop();
			return alert( resp.data );
		});

	}
};

login.init();
