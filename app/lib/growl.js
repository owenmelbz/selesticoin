Growl = function(message, timeout) {
    
    if( typeof message == "undefined" ) return false;
    if( typeof timeout == "undefined" ) var timeout = 3000;
    
    var timer;
    
    var window = Ti.UI.createWindow({
        title : 'Notification',
        bottom: '-60',
        height: 50,
        fullscreen: true
    });

    window.add( Ti.UI.createLabel({
        bottom : 0,
        color : '#FFF',
        opacity : 0.8,
        font : {
            fontWeight : 'bold'
        },
        backgroundColor : '#1e1e1e',
        text : message,
        textAlign : 'center',
        width : '100%',
        height : 50
    }) );
    
    var animation = Ti.UI.createAnimation({
        bottom: 0,
        duration: 1000
    });
    
    window.addEventListener('click', function(){
        clearTimeout(timer);
        
        var close = Ti.UI.createAnimation({
            bottom: '-60',
            duration: 1000
        });
        
        close.addEventListener('complete', function(){
           window = null; 
        });
        
        if(window != null) window.close(close);
        
    });
    
    animation.addEventListener('complete', function() { 
        var close = Ti.UI.createAnimation({
            bottom: '-60',
            duration: 1000
        });
        
        close.addEventListener('complete', function(){
           window = null; 
        });
        
        timer = setTimeout(function(){
           if(window != null ) window.close(close);
           window = null;
        }, timeout);
    });
    window.open(animation);
};

module.exports = Growl;