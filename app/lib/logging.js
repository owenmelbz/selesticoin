var Log = function(){
    
};
/**
 * Allows you to log data to the console
 * @param {object} Object Data you wish to log out
 * @return {function} Ti.API.log()
 */
Log.log = Log.info = function(e) {
    try {
        e = JSON.stringify(e);
    } catch(ex){}
    Ti.API.log(e);
};
/**
 * Allows you to log warn data to the console
 * @param {object} Object Data you wish to log out
 * @return {function} Ti.API.warn()
 */
Log.warn = function(e) {
    try {
        e = JSON.stringify(e);
    } catch(ex){}
    Ti.API.warn(e);
};
/**
 * Allows you to log error data to the console
 * @param {object} Object Data you wish to log out
 * @return {function} Ti.API.error()
 */
Log.error = function(e) {   
    try{
        e = JSON.stringify(e);
    } catch(ex){}
    Ti.API.error(e);
};


Log.toFile = function(data){
    
    try {
        var Logs = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,'Logs');
        if(!Logs.exists()) Logs.createDirectory();
            
        var log = Titanium.Filesystem.getFile(Logs.nativePath,'Logs.txt');
        if(!log.exists()) log.createFile();
        
        if (log.exists()){
            log.write( log.read() + '[ ' + Moment().format('MMMM Do YYYY, h:mm:ss a') +' ] '+ JSON.stringify(data) + '\n');
        }
    } catch(e){
        Ti.API.error('Couldnt Write Error To File: ' + data);
    }
    
    try {
    	
    	GA.trackEvent({
    		category: "Error Log",
    		action: "error",
    		label: "internal/api errors",
    		value: 1
    	});
    	
    } catch(e){
    	
    }
};

Log.purge = function(){
    var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory+'/Logs','Logs.txt');
    if (file.exists()) { file.deleteFile(); }
    
    var Logs = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,'Logs');
    if(!Logs.exists()) Logs.createDirectory();
            
    var log = Titanium.Filesystem.getFile(Logs.nativePath,'Logs.txt');
    if(!log.exists()) log.createFile();
};

Log.send = function( filePath ){
    
    var log = filePath || Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory+'/Logs','Logs.txt');
    if ( log.exists()) {
    
        var data = {
            type: Ti.App.deployType,
            id: Ti.App.installId,
            keyboardVisible: Ti.App.keyboardVisible,
            idleTimerDisabled: Ti.App.idleTimerDisabled,
            version: Ti.App.version,
            log: log.read()
        };
        
        var success = function(resp){
            Ti.API.error('Log Submitted');
        };
        
        var fail = function(resp){
            Ti.API.error('Couldnt Submit');
        };
        
        var Post = new XHR();
        
        Post.post(
            Alloy.CFG.logURL,
            data,
            success,
            fail,
            {
                shouldAuthenticate: false,
                contentType: 'application/json'
            }
        );
    
    } else {
        Ti.API.error('Nothing To Report');
    }
    
};

module.exports = Log;
