checkConnectivity = function() {
    
    if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
        Ti.connected = false;
    } else {
        Ti.connected = true;
    }
    
    setInterval(function() {
        if (Titanium.Network.networkType === Titanium.Network.NETWORK_NONE) {
            Ti.connected = false;
        } else {
            Ti.connected = true;
        }
    }, 1000);
};
checkConnectivity();

app.clearCaches = function(){
    
    Log.info('[CACHE] Clearing Images');
    
    var imageCache = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'cachedImages');
    if(imageCache.exists()) imageCache.deleteDirectory(true);
    
};
