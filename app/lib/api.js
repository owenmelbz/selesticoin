var Api = function () {

    /* Define some internals */
    var endpoint = Alloy.CFG.api;

    /* Category Based Calls */
    this.auth = {

        login    : endpoint + 'auth',

        register : endpoint + 'user',

    };

    this.boards = {
       
        mine: endpoint + 'board',
       
		new	: endpoint + 'board',

    	get	: function( id ){
    		return endpoint + 'board/' + id;
    	},

    	score : function( board , user ){
    		return endpoint + 'board/' + board + '/score/' + user;
    	}

    };

};

module.exports = Api;