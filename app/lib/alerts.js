Alert = function(message, title) {
    
    var title = typeof title != "undefined" ? title : 'Oops';
    
    var alert = Ti.UI.createAlertDialog({
            title   : title,
            message : message
    }).show();
};

module.exports = Alert;
