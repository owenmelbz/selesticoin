var OldLoader = function(){
    /**
     * Triggers a loading animation
     * @extends app.loadingAni
     * @param {String} LoadingMessage Displays this message on the spinner
     *
     */

    loadingContainer = loadingOverlay = loadingAni = null;
    this.start = function(message) {
        if( app.isAndroid ) return true;
        if ( typeof message == 'undefined') {
                message = "Loading";
        }

        if (loadingContainer != null && typeof (loadingContainer.children[0]) != 'undefined') {
            this.stop();
        }
        loadingContainer = Titanium.UI.createWindow({
            backgroundColor : app.loggedIn ? "#75000000" : 'white',
            fullscreen: true
        });

        loadingContainer.addEventListener('longpress', Loader.stop);

        loadingOverlay = Titanium.UI.createImageView({
            image: '/coin0.png',
			images: [
				"/coin0.png",
				"/coin1.png",
				"/coin2.png",
				"/coin3.png"
			],
			repeatCount: 0,
			duration: 125,
			width: 51,
			height: 58
        });

        loadingOverlay.start();

        loadingContainer.add(loadingOverlay);
        loadingContainer.open();
    };
    /**
     * Stops the loading animation
     * @extends app.loadingAni
     */
    this.stop = function() {
        if( app.isAndroid ) return true;
        if ( loadingContainer != null && typeof loadingContainer != "undefined" && typeof (loadingContainer.children[0]) != 'undefined') {
            loadingContainer.removeAllChildren();
            loadingContainer.close();
            loadingContainer.removeEventListener('longpress', Loader.stop);
        }
    };
};

Loader = {
    
    window: null,
    animation: null,
    
    start: function(){
        
        if( !Loader.window ){
            Loader.window = Titanium.UI.createWindow({
                backgroundColor : app.loggedIn ? ( app.isAndroid ? 'white' : '#75000000' ) : 'white',
                fullscreen: app.isAndroid ? false : true,
                modal: app.isAndroid ? true : false,
                exitOnClose: false
            });
            
            Loader.window.addEventListener('longpress', Loader.stop);
        } else {
            Loader.window.backgroundColor = app.loggedIn ? ( app.isAndroid ? 'white' : '#75000000' ) : 'white';
        }
        
        if( !Loader.animation ){
            
            Loader.animation = Titanium.UI.createImageView({
                images: [
                    "/coin0.png",
                    "/coin1.png",
                    "/coin2.png",
                    "/coin3.png"
                ],
                repeatCount: 0,
                duration: 125,
                width: 51,
                height: 58
            });
            
            Loader.window.add( Loader.animation );
        }
        
        Loader.window.open();
        Loader.animation.start();
        
    },
    
    stop: function(){ 
        if( Loader.window && Loader.animation ){
            Loader.window.close();
        } 
    }
    
}

module.exports = Loader;