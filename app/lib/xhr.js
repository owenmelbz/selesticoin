// Create the cache manager (a shared object)
var cacheManager = Titanium.App.Properties.getObject("cachedXHRDocuments", {});

XHR = function() {
};

// Public functions
// ================

// GET
// @url (string) URL to fetch
// @onSuccess (function) success callback
// @onError (function) error callback
// @extraParams (object)
XHR.prototype.get = function(url, onSuccess, onError, extraParams) {
	// Debug
	Titanium.API.warn('[XHR GET] ' + url);

	// Create some default params
	var onSuccess = onSuccess ||
	function() {
	};
	var onError = onError ||
	function() {
	};
	var extraParams = extraParams || {};
	extraParams.async = (extraParams.hasOwnProperty('async')) ? extraParams.async : true;
	extraParams.ttl = extraParams.ttl || false;
	extraParams.shouldAuthenticate = typeof extraParams.shouldAuthenticate != "undefined" ? extraParams.shouldAuthenticate : true;
	// if you set this to true, pass "username" and "password" as well
	extraParams.contentType = extraParams.contentType || "application/json";

	var cache = readCache(url);
	// If there is nothing cached, send the request
	if (!extraParams.ttl || cache == 0) {

		// Create the HTTP connection
		var xhr = Titanium.Network.createHTTPClient({
			enableKeepAlive : false
		});
		// Create the result object
		var result = {};

		// Open the HTTP connection
		xhr.open("GET", url, extraParams.async);
		xhr.setRequestHeader('Content-Type', extraParams.contentType);

		// If we need to authenticate
        if( app.user.token ) xhr.setRequestHeader('key', app.user.token);

		// When the connection was successful
		xhr.onload = function() {
			// Check the status of this
			result.status = xhr.status >= 200 && xhr.status < 400 ? "ok" : xhr.status;

			// Check the type of content we should serve back to the user
			if (extraParams.contentType.indexOf("application/json") != -1) {
				result.data = xhr.responseText;
			} else if (extraParams.contentType.indexOf("text/xml") != -1) {
				result.data = xhr.responseXML;
			} else {
				result.data = xhr.responseData;
			}

			onSuccess(result);

			// Cache this response
			writeCache(result.data, url, extraParams.ttl);
		};

		// When there was an error
		xhr.onerror = function(e) {
			// Check the status of this
			result.status = "error";
			result.error = e.error;
			result.code = xhr.status;
			result.data = xhr.responseText;
			result.blob = e;

			onError(result);

		};

		xhr.send();
	} else {
		var result = {};

		result.status = "cache";
		result.data = cache;

		onSuccess(result);
	}
};

// POST requests
// @url (string) URL to fetch
// @data (object)
// @onSuccess (function) success callback
// @onError (function) error callback
// @extraParams (object)
XHR.prototype.post = function(url, data, onSuccess, onError, extraParams) {

	// Debug
	Titanium.API.warn('[XHR POST] ' + url + " " + JSON.stringify(data));

	// Create some default params
	var onSuccess = onSuccess ||function() {};
	var onError = onError || function() {};
	var extraParams = extraParams || {};
	extraParams.async = (extraParams.hasOwnProperty('async')) ? extraParams.async : true;
	extraParams.shouldAuthenticate = typeof extraParams.shouldAuthenticate != "undefined" ? extraParams.shouldAuthenticate : true;
	// if you set this to true, pass "username" and "password" as well
	extraParams.contentType = extraParams.contentType || "application/json";

	// Create the HTTP connection
	var xhr = Titanium.Network.createHTTPClient({
		enableKeepAlive : false
	});
	// Create the result object
	var result = {};

	// Open the HTTP connection
	xhr.open("POST", url, extraParams.async);

	// If we need to authenticate
	if( app.user.token ) xhr.setRequestHeader('key', app.user.token);

	// When the connection was successful
	xhr.onload = function() {
		// Check the status of this
		result.status = xhr.status >= 200 && xhr.status < 400 ? "ok" : xhr.status;
        
		result.data = xhr.responseText;

		onSuccess(result);
	};

	// When there was an error
	xhr.onerror = function(e) {
        
		// Check the status of this
		result.status = "error";
		result.error = e.error;
		result.code = xhr.status;
		result.data = xhr.responseText;
		onError(result);
	};

	if (extraParams.contentType == "application/json") data = JSON.stringify(data);

	xhr.setRequestHeader('Content-Type', extraParams.contentType);
	xhr.send(data);
};

// PUT requests
// @url (string) URL to fetch
// @data (object)
// @onSuccess (function) success callback
// @onError (function) error callback
// @extraParams (object)
XHR.prototype.put = function(url, data, onSuccess, onError, extraParams) {

	// Debug
	Titanium.API.warn('[XHR PUT] ' + url + " " + JSON.stringify(data));

	// Create some default params
	var onSuccess = onSuccess ||
	function() {
	};
	var onError = onError ||
	function() {
	};
	var extraParams = extraParams || {};
	extraParams.async = (extraParams.hasOwnProperty('async')) ? extraParams.async : true;
	extraParams.shouldAuthenticate = typeof extraParams.shouldAuthenticate != "undefined" ? extraParams.shouldAuthenticate : true;
	// if you set this to true, pass "username" and "password" as well
	extraParams.contentType = extraParams.contentType || "application/json";

	// Create the HTTP connection
	var xhr = Titanium.Network.createHTTPClient({
		enableKeepAlive : false
	});
	// Create the result object
	var result = {};

	// Open the HTTP connection
	xhr.open("PUT", url, extraParams.async);

	// If we need to authenticate
	if( app.user.token ) xhr.setRequestHeader('key', app.user.token);

	// When the connection was successful
	xhr.onload = function() {
		// Check the status of this
		result.status = xhr.status >= 200 && xhr.status < 400 ? "ok" : xhr.status;
		result.data = xhr.responseText;

		onSuccess(result);
	};

	// When there was an error
	xhr.onerror = function(e) {
		// Check the status of this
		result.status = "error";
		result.error = e.error;
		result.code = xhr.status;
		result.data = xhr.responseText;

		onError(result);

	};

	if (extraParams.contentType == "application/json")
		data = JSON.stringify(data);

	xhr.setRequestHeader('Content-Type', extraParams.contentType);
	xhr.send(data);
};

// DELETE requests
// @url (string) URL to fetch
// @onSuccess (function) success callback
// @onError (function) error callback
// @extraParams (object)
XHR.prototype.destroy = function(url, onSuccess, onError, extraParams) {

	// Debug
	Titanium.API.warn('[XHR DESTORY] ' + url);

	// Create some default params
	var onSuccess = onSuccess ||
	function() {
	};
	var onError = onError ||
	function() {
	};
	var extraParams = extraParams || {};
	extraParams.async = (extraParams.hasOwnProperty('async')) ? extraParams.async : true;
	extraParams.shouldAuthenticate = typeof extraParams.shouldAuthenticate != "undefined" ? extraParams.shouldAuthenticate : true;
	// if you set this to true, pass "username" and "password" as well
	extraParams.contentType = extraParams.contentType || "application/json";

	// Create the HTTP connection
	var xhr = Titanium.Network.createHTTPClient({
		enableKeepAlive : false
	});
	// Create the result object
	var result = {};

	// Open the HTTP connection
	xhr.open("DELETE", url);

	// If we need to authenticate
	if( app.user.token ) xhr.setRequestHeader('key', app.user.token);

	// When the connection was successful
	xhr.onload = function() {
		// Check the status of this
		result.status = xhr.status >= 200 && xhr.status < 400 ? "ok" : xhr.status;
		result.data = xhr.responseText;
		Log.warn(result);
		onSuccess(result);
	};

	// When there was an error
	xhr.onerror = function(e) {

		// Check the status of this
		result.status = "error";
		result.error = e.error;
		result.code = xhr.status;
		result.data = xhr.responseText;

		Log.error(result);

		onError(result);

	};
	xhr.send();
};

// Helper functions
// =================

// Removes the cached content of a given URL (this is useful if you are not satisfied with the data returned that time)
XHR.prototype.clear = function(url) {

	if (url) {
		// Hash the URL
		var hashedURL = Titanium.Utils.md5HexDigest(url);
		// Check if the file exists in the manager
		var cache = cacheManager[hashedURL];

		// If the file was found
		if (cache) {
			// Delete references and file
			var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, hashedURL);
			// Delete the record and file
			delete cacheManager[hashedURL];
			file.deleteFile();

			// Update the cache manager
			updateCacheManager();

			//Titanium.API.info("REMOVED CACHE FILE " + hashedURL);
		}
	}

};

// Removes all the expired documents from the manager and the file system
XHR.prototype.clean = function() {

	var nowInMilliseconds = new Date().getTime();
	var expiredDocuments = 0;

	for (var key in cacheManager) {
		var cache = cacheManager[key];

		if (cache.timestamp <= nowInMilliseconds) {
			// Delete references and file
			var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, key);
			// Delete the record and file
			delete cacheManager[key];
			file.deleteFile();

			// Update the cache manager
			updateCacheManager();

			// Update the deleted documents count
			expiredDocuments = expiredDocuments + 1;

			//Titanium.API.info("REMOVED CACHE FILE " + cachedDocuments[i].file);
		}

	}

	// Return the number of files deleted
	return expiredDocuments;
};

// Removes all documents from the manager and the file system
XHR.prototype.purge = function() {

	var purgedDocuments = 0;

	for (var key in cacheManager) {
		var cache = cacheManager[key];
		// Delete references and file
		var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, key);
		// Delete the record and file
		delete cacheManager[key];
		file.deleteFile();

		// Update the cache manager
		updateCacheManager();

		// Update the deleted documents count
		purgedDocuments = purgedDocuments + 1;

		//Titanium.API.info("REMOVED CACHE FILE " + cachedDocuments[i].file);

	}

	// Return the number of files deleted
	return purgedDocuments;
};

XHR.prototype.download = function(data, onSuccess, onError, extraParams) {

	// Create some default params
	var onSuccess = onSuccess ||
	function() {
	};
	var onError = onError ||
	function() {
	};
	var onProgress = onProgress ||
	function() {
	};
	var extraParams = extraParams || {};
	extraParams.shouldAuthenticate = typeof extraParams.shouldAuthenticate != "undefined" ? extraParams.shouldAuthenticate : true;
	// if you set this to true, pass "username" and "password" as well

	if (data.fullPath) {

		var directory = Ti.Filesystem.createDirectory('downloads/' + data.fullPath);
		var f = Titanium.Filesystem.getFile(directory.nativePath, Ti.Utils.md5HexDigest(data.url));

	} else {

		var hex = Ti.Utils.md5HexDigest(data.url);
		var fullPath = "organisation-" + data.org + "/" + data.dir + "/" + hex;
		var directory = Ti.Filesystem.createDirectory('downloads/' + fullPath);

		var f = Titanium.Filesystem.getFile(directory.nativePath, data.name || hex);
	}
	if (f.exists()) {
		Log.info('[ DOC ] Found Cached Version');
		return onSuccess(f.nativePath);
	} else if (Titanium.Network.online) {
		Log.info('[ DOC ] Downloading ' + data.url);
		// Create the HTTP connection
		var xhr = Titanium.Network.createHTTPClient({
			enableKeepAlive : true
		});

		// Create the result object
		var result = {};

		// Open the HTTP connection
		xhr.open("GET", data.url);
		xhr.setFile(f);

		//xhr.file = f;

		// If we need to authenticate
		if (extraParams.shouldAuthenticate) {
			var authstr = Alloy.CFG.ApiUsername + ' ' + Alloy.CFG.ApiPassword;
			xhr.setRequestHeader('Authorization', authstr);
		}

		// When the connection was successful
		xhr.onload = function() {
			// Check the status of this
			Log.info(xhr);
			result.status = xhr.status >= 200 && xhr.status < 400 ? "ok" : xhr.status;

			if (result.status == "ok") {

				if (data.useExtension) {
					var extension = Ti.Utils.mimeToExt(xhr.getResponseHeader('Content-Type'));
					var newFilePath = f.nativePath + "." + extension;
					var renamed = f.rename(f.getName() + "." + extension);
					if (!renamed)
						f.deleteFile();
					f = Ti.Filesystem.getFile(newFilePath);
				}

				onSuccess(f.nativePath);

			} else {
				result.status = "error";
				result.code = xhr.status;
				result.text = xhr.responseText;
				result.data = xhr.responseData;
				onError(result);
			}
		};

		xhr.ondatastream = function(e) {
			Log.info(e.progress);
			onProgress(e.progress);
		};

		// When there was an error
		xhr.onerror = function(e) {
			// Check the status of this
			result.status = "error";
			result.error = e.error;
			result.code = xhr.status;
			result.data = xhr.responseData;
			result.text = xhr.responseText;

			onError(result);

		};

		xhr.send();
	} else {
		Log.info("no connection");
		result.status = "error";
		result.error = "Not connected to the internet";
		result.code = "000";
		result.data = null;
		result.text = null;
		onError(result);
	}
};

// Private functions
// =================

readCache = function(url) {
	// Hash the URL
	var hashedURL = Titanium.Utils.md5HexDigest(url);

	// Check if the file exists in the manager
	var cache = cacheManager[hashedURL];
	// Default the return value to false
	var result = false;

	//Titanium.API.info("CHECKING CACHE");

	// If the file was found
	if (cache) {
		// Fetch a reference to the cache file
		var directory = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'cachedXHR');
		var file = Titanium.Filesystem.getFile(directory.nativePath, hashedURL);

		// Check that the TTL is further than the current date
		if (cache.timestamp >= new Date().getTime()) {
			//Titanium.API.info("CACHE FOUND");

			// Return the content of the file
			result = file.read();

		} else {
			//Titanium.API.info("OLD CACHE");

			// Delete the record and file
			delete cacheManager[hashedURL];
			file.deleteFile();

			// Update the cache manager
			updateCacheManager();
		}
	} else {
		//Titanium.API.info("CACHE " + hashedURL + " NOT FOUND");
	}

	return result;
};

updateCacheManager = function() {
	Titanium.App.Properties.setObject("cachedXHRDocuments", cacheManager);
};

writeCache = function(data, url, ttl) {

	//Titanium.API.info("WRITING CACHE");
	var directory = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'cachedXHR');
	if (!directory.exists())
		directory.createDirectory();

	// hash the url
	var hashedURL = Titanium.Utils.md5HexDigest(url);

	// Write the file to the disk
	var file = Titanium.Filesystem.getFile(directory.nativePath, hashedURL);

	// Write the file to the disk
	// T0DO: There appears to be a bug in Titanium and makes the method
	// below always return false when dealing with binary files
	file.write(data);

	// Insert the cached object in the cache manager
	cacheManager[hashedURL] = {
		"timestamp" : (new Date().getTime()) + (ttl * 60 * 1000)
	};
	updateCacheManager();

	//Titanium.API.info("WROTE CACHE");
};

// Return everything
module.exports = XHR;