/**
 * Capitalizes the first character of a string
 * @return {String} Capitalized string
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

/**
 * Returns a automated humanisation of a nasty string
 * @return {String} Humanized string
 */
String.prototype.humanize = function() {
    return this.replace(/([A-Z])/g, ' $1').replace(/^./, function(str){ return str.toUpperCase(); }).replace('_', ' ');
};

/**
 * Truncates a string based on N
 * @return {String} Capitalized string
 */
String.prototype.trunc = String.prototype.trunc || function(n, ending) {
    var ending = typeof ending != 'undefined' ? ending : '...'.toString();
    return this.length > n ? this.substr(0, n - 1) + ending : this;
};

Ti.Utils.getExtension = function(fn) {
    var re = /(?:\.([^.]+))?$/;
    var tmpext = re.exec(fn)[1];
    return (tmpext) ? tmpext : '';
};

Ti.UI.createCachedImage = function(a) {
    a = a || {};
    var md5;
    var needsToSave = false;
    var savedFile;

    if ( a.image ) {

        md5 = Ti.Utils.md5HexDigest( a.image ) + '.' + Ti.Utils.getExtension( a.image );

        var dir = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, "cachedImages");
    	if(!dir.exists()) dir.createDirectory();

        savedFile = Titanium.Filesystem.getFile(dir.nativePath, md5);

        if ( savedFile.exists() ) {
            a.image = savedFile.nativePath;
        } else {
            needsToSave = true;
        }
    }

    var image = Ti.UI.createImageView( a );

    if (needsToSave === true) {

        function saveImage(e) {

            image.removeEventListener('load', saveImage);

            savedFile.write( Ti.UI.createImageView({
                image : image.image,
                width : 'auto',
                height : 'auto'
            }).toImage() );
        }

        image.addEventListener('load', saveImage);
    }

    return image;
};

Ti.Utils.validEmail = function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

Ti.Utils.numberFormat = function( number, decimals, dec_point, thousands_sep ){
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

Ti.Utils.fileSize = function(filesize){
    var sizeStr;
    if (filesize >= 1073741824) {
      sizeStr = Ti.Utils.numberFormat(filesize / 1073741824, 2, ".") + " GB";
    } else if (filesize >= 1048576) {
      sizeStr = Ti.Utils.numberFormat(filesize / 1048576, 2, ".") + " MB";
    } else if (filesize >= 1024) {
      sizeStr = Ti.Utils.numberFormat(filesize / 1024, 0) + " KB";
    } else {
      sizeStr = Ti.Utils.numberFormat(filesize, 0) + " bytes";
    }
    return sizeStr;
};

Ti.Utils.mimeToExt = function(mimeType) {

    var types = {
        // -- VIDEO TYPES --
        "video/mp4" : "mp4",
        "video/mpeg" : "mpg",
        "video/quicktime" : "mov",
        "video/x-msvideo" : "avi",
        "video/ogg" : "ogv",
        "video/3gpp" : "3gpp",

        // -- AUDIO TYPES --
        "audio/wav" : "wav",
        "audio/mpeg" : "mp3",
        "audio/ogg" : "ogg",

        // -- IMAGE TYPES --
        "image/jpeg" : "jpg",
        "image/gif" : "gif",
        "image/png" : "png"
    };

    return typeof types[mimeType] != "undefined" ? types[mimeType] : undefined;

};

Ti.Filesystem.createDirectory = function( path ){

    var folders = path.split("/");
    var builtPath = Ti.Filesystem.applicationDataDirectory;

    _.each(folders, function(folder){
        var dir = Ti.Filesystem.getFile(builtPath, folder);
        if( dir.exists() && dir.isDirectory() ) builtPath = dir.nativePath;
        else {
            dir.createDirectory();
            builtPath = dir.nativePath;
        }

    });

    return Ti.Filesystem.getFile(builtPath);
};

Array.prototype.chunk = function(chunkSize) {
    var R = [];
    for (var i=0; i<this.length; i+=chunkSize)
        R.push(this.slice(i,i+chunkSize));
    return R;
};
