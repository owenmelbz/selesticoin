var app = {},
	index = {},
	sB = {};

Alloy.Globals.Font = Ti.Platform.osname == 'android' ? 'SuperMario256' : 'Super Mario 256';
app.isAndroid = Ti.Platform.osname == 'android' ? true : false;

app.init = function(){
	app.installId = Titanium.Platform.id;
	app.user = Titanium.App.Properties.getObject('user', {});
	app.loggedIn = false
	app.Notifications.subscribe();

	this.checkLogin();
};

app.checkLogin = function(){

	Loader.start('Loading');

	new XHR().post( Api.auth.login , {
		device: app.installId,
		token: app.Notifications.token
	}, function( resp ){
		app.user = JSON.parse( resp.data );
		Titanium.App.Properties.setObject('user', app.user);
		app.loginSuccess();
		//Loader.stop();
	}, function( resp ){
	    app.allowedAuth = JSON.parse(resp).allowedAuth;
		app.openAuth( resp.allowedAuth );
		Loader.stop();
	});
};

app.openAuth = function(){

	app.loginWindow = Ti.UI.createWindow({
		fullscreen: true,
		id: 'loginWindow',
		backgroundColor: 'white'
	});

	app.loginWindow.add( Alloy.createWidget('login').getView() );
	app.loginWindow.open();
};

app.loginSuccess = function(){
	index.init();

	if(app.loginWindow){
		app.loginWindow.close();
		app.loginWindow = null;
	}

	Log.warn(app.user);
	app.loggedIn = true;

	Titanium.App.Properties.setString('last_username', app.user.displayname);
};

app.loginFail = function(){

};

app.Notifications = {

	token: null,

	subscribe: function(){

		app.Notifications.token = Titanium.App.Properties.getString('deviceToken', null);

		if( app.isAndroid ) app.Notifications.android();
		else app.Notifications.ios();
	},

	ios: function(){

		Ti.Network.registerForPushNotifications({
		    types: [
		        Ti.Network.NOTIFICATION_TYPE_BADGE,
		        Ti.Network.NOTIFICATION_TYPE_ALERT,
		        Ti.Network.NOTIFICATION_TYPE_SOUND
		    ],
		    success: app.Notifications.success,
		    error: app.Notifications.error,
		    callback: app.Notifications.recieved
		});

	},

	android: function(){
	    
	    var gcm = require('net.iamyellow.gcmjs');
	    
	    var pendingData = gcm.data;
        if (pendingData && pendingData !== null) {
            // if we're here is because user has clicked on the notification
            // and we set extras for the intent 
            // and the app WAS NOT running
            // (don't worry, we'll see more of this later)
            Ti.API.info('******* data (started) ' + JSON.stringify(pendingData));
        }

		gcm.registerForPushNotifications({
            success: app.Notifications.success,
            error: app.Notifications.error,
            callback: app.Notifications.recieved,
            unregister: function (ev) {
                // on unregister 
                Ti.API.info('******* unregister, ' + ev.deviceToken);
            },
            data: function (data) {
                // if we're here is because user has clicked on the notification
                // and we set extras in the intent 
                // and the app WAS RUNNING (=> RESUMED)
                // (again don't worry, we'll see more of this later)
                Ti.API.info('******* data (resumed) ' + JSON.stringify(data));
            }
       });
	},

	recieved: function( e ){
		if( typeof sB.refreshBoard != "undefined" ){
		    sB.refreshBoard();
		}
		alert( (e.data && e.data.alert) || e.message);
	},

	error: function( e ){
	    //alert(e);
		Log.error( e );
	},

	success: function( e ){
	    
		app.Notifications.token = e.deviceToken;

		var stored = Titanium.App.Properties.getString('deviceToken', null);
		if( !stored ) Titanium.App.Properties.setString('deviceToken', app.Notifications.token);

	}

};

/* Include our Librarys Please */
var Moment = require('/moment'); // http://momentjs.com/
var Growl = require('/growl');
var Log = require('/logging');
var Alert = require('/alerts');
var Loader = require('/loader');
var XHR = require('/xhr');
var Api = require('/api'); Api = new Api();

require('/extend');
require('/utilities');



app.init();
