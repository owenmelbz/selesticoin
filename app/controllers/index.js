index.init = function(){
	index.boards = {};
	index.scroller = $.mainScroller;
	index.tableRows = [];
	index.preLayout();
	index.activeShroom = null;
};

index.bindEvents = function(){
	$.myBoards.addEventListener('click', index.boardManagement.openBoard);
	$.myBoards.addEventListener('longpress', index.boardManagement.leaveBoard);
	$.myBoards.addEventListener('swipe', function(e){
        if( e.direction == "up" || e.direction == "down" ) index.refreshBoards();
    });
    $.myBoards.addEventListener('dragend', index.refreshBoards);
	$.joinBoard.addEventListener('click', index.boardManagement.createBoard);
};

index.preLayout = function(){

	index.loadBoards();
	$.joinBoard.transform = Ti.UI.create2DMatrix().rotate('-3');
	index.postLayout();
};

index.postLayout = function(){
	index.bindEvents();
	$.index.open();
};

index.loadBoards = function(){
	index.boards = app.user.boards;

	_.each( index.boards , function( board , k ){
		index.boardManagement.createBoardRow( board , true , !!k );
	});

	$.myBoards.setData( index.tableRows );
};

index.boardManagement = {

	createBoardRow: function( board , make , isFirst ){

		make = make || true;
		isFirst = isFirst || false;

		board.row = Ti.UI.createTableViewRow({
	        boardId: board.id,
            className:'boardRow',
            backgroundColor: 'transparent',
            backgroundImage: 'none',
            height: 50,
            top: isFirst ? 10 : 0
		});

		board.view = Ti.UI.createView({
		    touchEnabled: false,
            backgroundColor: 'transparent',
            backgroundImage: 'none',
            height: 50
		});

		board.view.add( Ti.UI.createLabel({
		    height: 40,
		    top: 7,
		    text: board.name,
		    color: '#000',
            textAlign: 'center',
            font: {
                fontFamily: Alloy.Globals.Font,
                fontSize: 25
            },
            touchEnabled: false,
            transform: Ti.UI.create2DMatrix().rotate('1')
		}) );

		board.view.add( Ti.UI.createLabel({
            height: 40,
            text: board.name,
            color: '#fff',
            textAlign: 'center',
            font: {
                fontFamily: Alloy.Globals.Font,
                fontSize: 24
            },
            touchEnabled: false,
        }) );
    
        if(!app.isAndroid){
    		board.view.add( Ti.UI.createView({
    		    height: 1,
    		    width: '90%',
    		    bottom: 5,
    		    backgroundColor: 'white',
    		    touchEnabled: false,
    		}) );
		}

		board.shroom = Ti.UI.createImageView({
		    image: '/mushroom_red.png',
		    visible: false,
		    isShroom: true,
		    top: 10,
		    left: 15,
		    height: 20,
		    width: 20
		});

		board.view.add( board.shroom );
		board.row.add( board.view );

		if( make ) index.tableRows.push( board.row );
		else $.myBoards.appendRow( board.row );
	},

	openBoard: function( event , refresh){
		if( event.row && event.row.boardId ){

			Loader.start('Loading');

			var success = function( resp ){

			    var rowChildren = event.row.getChildren()[0].getChildren();
			    var shroom = _.findWhere( rowChildren , { isShroom: true } );
	                shroom.visible = true;

	            if(index.activeShroom) index.activeShroom.visible = false;
	            index.activeShroom = shroom;

				$.scoreScreen.removeAllChildren();
				$.scoreScreen.add( Alloy.createController('scoreBoard', { board: JSON.parse( resp.data ) }).getView() );
				$.mainScroller.scrollToView( $.scoreScreen );
				$.mainScroller.scrollingEnabled = true;
			};

			var fail = function(resp){
				alert(resp.data);
				Loader.stop();
			};

			new XHR().get( Api.boards.get( event.row.boardId ), success, fail);
		}
	},

	leaveBoard: function( event ){
		if( event.row && event.row.boardId ){

			var dialog = Ti.UI.createAlertDialog({
				cancel: 1,
				buttonNames: ['Leave', 'Cancel'],
				message: 'Would you like to leave this board? Your coins will be lost',
				title: 'Leave Board'
			});

			dialog.addEventListener('click', function(e){
				if (e.index != e.source.cancel){
				    
				    Loader.start('Leaving');

					var success = function(){
						$.myBoards.deleteRow( event.row );
						$.mainScroller.scrollingEnabled = false;
						Loader.stop();
					};

					var fail = function( resp ){
						alert(resp.data);
						Loader.stop();
					};

					new XHR().destroy( Api.boards.get( event.row.boardId ), success, fail);
				}
			});

			dialog.show();

		}
	},

	createBoard: function(){
        
        var a_input = Ti.UI.createTextField();
        
        if(app.isAndroid){
            
            var dialog = Ti.UI.createAlertDialog({
                cancel: 1,
                title: 'Enter Board Name.',
                message: 'If it doesnt exist, it will create it.',
                buttonNames: ['Join', 'Cancel'],
                androidView: a_input
            });
            
        } else {
            
            var dialog = Ti.UI.createAlertDialog({
                cancel: 1,
                title: 'Enter Board Name.',
                message: 'If it doesnt exist, it will create it.',
                style: Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,
                buttonNames: ['Join', 'Cancel']
            });
            
        }

		dialog.addEventListener('click', function(e){

			if ((e.index != e.source.cancel && e.text) || (e.index != e.source.cancel && a_input.value)){
                
                Loader.start('Joining');
                
                var boardName = e.text || a_input.value;
                
				var success = function(resp){

					resp = JSON.parse(resp.data);

					var newBoard = {
						name: resp.name,
						id: resp.id
					};

					var row = Ti.UI.createTableViewRow({
				        boardId: newBoard.id,
			            className:'boardRow',
			            backgroundColor: 'transparent',
			            backgroundImage: 'none',
			            height: 50,
			            top: 0
					});

					var view = Ti.UI.createView({
					    touchEnabled: false,
			            backgroundColor: 'transparent',
			            backgroundImage: 'none',
			            height: 50
					});

					view.add( Ti.UI.createLabel({
					    height: 40,
					    top: 7,
					    text: newBoard.name,
					    color: '#000',
			            textAlign: 'center',
			            font: {
			                fontFamily: Alloy.Globals.Font,
			                fontSize: 25
			            },
			            touchEnabled: false,
			            transform: Ti.UI.create2DMatrix().rotate('1')
					}) );

					view.add( Ti.UI.createLabel({
			            height: 40,
			            text: newBoard.name,
			            color: '#fff',
			            textAlign: 'center',
			            font: {
			                fontFamily: Alloy.Globals.Font,
			                fontSize: 24
			            },
			            touchEnabled: false,
			        }) );
                    
                    if( !app.isAndroid ){
    					view.add( Ti.UI.createView({
    					    height: 1,
    					    width: '90%',
    					    bottom: 5,
    					    backgroundColor: 'white',
    					    touchEnabled: false,
    					}) );
					}

					shroom = Ti.UI.createImageView({
					    image: '/mushroom_red.png',
					    visible: false,
					    isShroom: true,
					    top: 10,
					    left: 15,
					    height: 20,
					    width: 20
					});

					view.add( shroom );
					row.add( view );

					$.myBoards.appendRow( row );
					index.boards.push( row );
					
					Loader.stop();
				};

				var fail = function( resp ){
					alert(resp.data);
					Loader.stop();
				};

				new XHR().post( Api.boards.new , {
					board: boardName
				}, success, fail);
			}
		});

		dialog.show();
	}
};

index.refreshBoards = function(){
    
    Loader.start();
    
    var success = function( resp ){
        Loader.stop();
        app.user.boards = JSON.parse(resp.data);
        index.loadBoards
    };

    var fail = function( resp ){
        Loader.stop();
    };

    new XHR().get( Api.boards.mine, success, fail);
    
};

//index.init();
