sB.init = function( args ){
	sB.model = args.board;
	sB.board = $.activeBoard;
	sB.players = [];
	sB.preLayout();
	sB.editingPlayer = null;

	sB.coinSound = Ti.Media.createSound({url:"/coin.wav"});
	sB.failSound = Ti.Media.createSound({url:"/fail.wav"});
	sB.introSound = Ti.Media.createSound({url:"/loaded.wav"});
	sB.introSound.play();
};

sB.bindEvents = function(){
	sB.board.addEventListener('click', sB.editPlayer);
	sB.board.addEventListener('swipe', function(e){
	    if( e.direction == "up" || e.direction == "down" ) sB.refreshBoard();
	});
	sB.board.addEventListener('dragend', sB.refreshBoard);
	sB.openEditAnimation.addEventListener('complete', sB.editOpened);
	sB.closeEditAnimation.addEventListener('complete', sB.editClosed);
	$.editCoins.addEventListener('click', sB.closeEdit);
	$.editCoins.addEventListener('swipe', sB.adjustScore);
	$.saveNewCoins.addEventListener('click', sB.saveCoins);
};

sB.preLayout = function(){
	sB.loadBoard();
	sB.postLayout();
};

sB.postLayout = function(){
	sB.bindEvents();
	setTimeout(Loader.stop, 500);
	setInterval(sB.refreshBoard, 60000);
};

sB.loadBoard = function(){
	$.boardName.text = sB.model.name;
	sB.players = sB.model.users;
	sB.orderBoard();
};

sB.createRow = function( player , position ){

    position = parseInt((position + 1));

    switch( position ){
        case 1:
            var rColor = '#FFD700';
        break;
        case 2:
            var rColor = '#C0C0C0';
        break;
        case 3:
            var rColor = '#E59400';
        break;
        default:
            var rColor = '#fff';
        break;
    }

	player.row = Ti.UI.createTableViewRow({
		player: player,
		height: 40,
		backgroundColor: 'transparent',
		backgroundImage: 'none'
	});

	player.wrapper = Ti.UI.createView({
	    height: '100%',
	    width: '100%',
	    touchEnabled: true
	});

	player.positionLabel = Ti.UI.createLabel({
	    text: position,
	    left: 20,
	    color: rColor,
	    font: {
	        fontFamily: Alloy.Globals.Font,
	        fontSize: 18
	    },
        touchEnabled: false
	});

	player.positionLabelShadow = Ti.UI.createLabel({
        text: position,
        left: 20,
        color: 'black',
        font: {
            fontFamily: Alloy.Globals.Font,
            fontSize: 19
        },
        touchEnabled: false
   });

	player.nameLabel = Ti.UI.createLabel({
	    text: player.displayname,
	    left: 50,
	    color: rColor,
        font: {
            fontFamily: Alloy.Globals.Font,
            fontSize: 25
        },
        touchEnabled: false
	});

	player.nameLabelShadow = Ti.UI.createLabel({
        text: player.displayname,
        left: 50,
        color: 'black',
        font: {
            fontFamily: Alloy.Globals.Font,
            fontSize: 26
        },
        touchEnabled: false
    });

	player.coinsLabel = Ti.UI.createLabel({
	    text: player.coins,
	    right: 20,
	    color: rColor,
        font: {
            fontFamily: Alloy.Globals.Font,
            fontSize: 21
        },
        touchEnabled: false
	});

	player.coinsLabelShadow = Ti.UI.createLabel({
        text: player.coins,
        right: 20,
        color: 'black',
        font: {
            fontFamily: Alloy.Globals.Font,
            fontSize: 22
        },
        touchEnabled: false
    });

	player.wrapper.add( player.positionLabelShadow );
	player.wrapper.add( player.positionLabel );
	player.wrapper.add( player.nameLabelShadow );
	player.wrapper.add( player.nameLabel );
	player.wrapper.add( player.coinsLabelShadow );
	player.wrapper.add( player.coinsLabel );

	player.row.add( player.wrapper );

	sB.board.appendRow( player.row );
	return player.row;
};

sB.orderBoard = function(){

	sB.players = _.sortBy( sB.players , function( player ){
		return parseInt(player.coins);
	}).reverse();

	sB.board.data = [];

	_.each( sB.players , function( player , key ){
		player.row = sB.createRow( player , key );
	});

};

sB.editPlayer = function( event ){

	if( event.row && event.row.player ){
		sB.editingPlayer = _.findWhere( sB.players , { id: event.row.player.id } );
		$.PlayerName.text = sB.editingPlayer.displayname;
		$.PlayerCoins.value = sB.editingPlayer.coins;
		index.scroller.scrollingEnabled = false;
		$.editCoins.animate( sB.openEditAnimation );
	}
};

sB.saveCoins = function(){

    var higherLower = 'same';

    if( $.PlayerCoins.value && sB.editingPlayer.coins != $.PlayerCoins.value ) {

    	Loader.start('Syncing');

    	var success = function( resp ){
    		
    		sB.model = JSON.parse( resp.data );
    		sB.players = sB.model.users;

    		$.editCoins.animate( sB.closeEditAnimation );
			higherLower = parseInt(sB.editingPlayer.coins) < parseInt($.PlayerCoins.value) ? 'higher' : 'lower';
			sB.editingPlayer.coins = $.PlayerCoins.value;
			sB.orderBoard();

	        if( higherLower == "higher" ){
	            setTimeout(sB.playCoinHigher, 200);
	        } else if ( higherLower == "lower"){
	            setTimeout(sB.playCoinLower, 200);
	        }
	        Loader.stop();
    	};

		var fail = function( resp ){
			Loader.stop();
			alert(resp.data);
		};

		new XHR().post( Api.boards.score( sB.model.id , sB.editingPlayer.id ), {
			coins: $.PlayerCoins.value
		}, success, fail);
    }
};

sB.adjustScore = function(e){

	if( e.direction == "up" || e.direction == "right" ){
		$.PlayerCoins.value = ( parseFloat($.PlayerCoins.value) + 1 );
	} else {
		if( $.PlayerCoins.value != "0"){
			$.PlayerCoins.value = ( parseFloat($.PlayerCoins.value) - 1 );
		}
	}
};

sB.openEditAnimation = Ti.UI.createAnimation({
	top: 0,
	duratiom: 400
});

sB.closeEditAnimation = Ti.UI.createAnimation({
	top: Ti.Platform.displayCaps.platformHeight,
	duration: 200
});

sB.editClosed = function(){
	sB.editingPlayer = null;
	$.PlayerCoins.value = "";
	$.PlayerCoins.blur();
	index.scroller.scrollingEnabled = true;
};

sB.editOpened = function(){
	$.PlayerCoins.value = sB.editingPlayer.coins;
	index.scroller.scrollingEnabled = false;
};

sB.closeEdit = function( event ){
    if( event.source.id != 'editCoins' ) return false;
	$.PlayerCoins.blur();
	index.scroller.scrollingEnabled = true;
	$.editCoins.animate( sB.closeEditAnimation );
};

sB.playCoinHigher = function(){

    $.coinAni.start();
    $.coinAni.visible = true;
    setTimeout(function(){
        sB.coinSound.play();
    }, 400);

    var coinJump = Ti.UI.createAnimation({
        top: '20%',
        duration: 600
    });

    coinJump.addEventListener('complete', function(){
        $.coinAni.animate( coinFall );
    });

    var coinFall = Ti.UI.createAnimation({
        top: Ti.Platform.displayCaps.platformHeight,
        duration: 500
    });

    coinFall.addEventListener('complete', function(){
        $.coinAni.stop();
        $.coinAni.visible = false;
    });

    $.coinAni.animate( coinJump );
};

sB.playCoinLower = function(){

    $.failAni.start();
    $.failAni.visible = true;
    sB.failSound.play();

    var coinJump = Ti.UI.createAnimation({
        left: '-50',
        duration: 2000
    });

    coinJump.addEventListener('complete', function(){
        $.failAni.stop();
        $.failAni.visible = false;
        $.failAni.left = Ti.Platform.displayCaps.platformWidth;
    });

    $.failAni.animate( coinJump );
};

sB.refreshBoard = function(){
    
    Loader.start();
    
    var success = function( resp ){
        Loader.stop();
        sB.model = JSON.parse( resp.data );
        sB.players = sB.model.users;
        sB.orderBoard();
    };

    var fail = function( resp ){
        Loader.stop();
    };

    new XHR().get( Api.boards.get( sB.model.id ), success, fail);
};

sB.init( (arguments[0] || {}) );
